<?php



interface CanFly {
    public function fly();
}

interface CanSwim {
    public function swim();
}

class Bird {
    //public $name;
    public function info() {
        echo "I am a {$this->name}\n";
        echo "I am an bird\n";
    }
}

class Penguine extends Bird implements CanSwim{
public $name="penguine";
    public function swim()
    {
       echo"i can swim<br>"; // TODO: Implement swim() method.
    }
}
class Dove extends Bird implements CanFly{
    public $name="Dove";
    public function fly()
    {
       echo"i can fly<br>"; // TODO: Implement fly() method.
    }
}
class Duck extends Bird implements CanFly,CanSwim{
    public $name="Duck";
    public function fly()
    {
        echo"i can fly<br>"; // TODO: Implement fly() method.
    }
    public function Swim()
    {
        echo"i can swim<br>"; // TODO: Implement fly() method.
    }
}
function describe($bird) {
    if ($bird instanceof Bird) {
        $bird->info();
        if ($bird instanceof CanFly) {
            $bird->fly();
        }
        if ($bird instanceof CanSwim) {
            $bird->swim();
        }
    } else {
        die("This is not a bird. I cannot describe it.");
    }
}

// describe these birds please
describe(new Penguine);
echo "***\n";

describe(new Dove);
echo "***\n";

describe(new Duck);

